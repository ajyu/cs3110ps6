open Definitions

type attacks = attackable_object Queue.t
type moves = position Queue.t

(* First team in game_data is red, second blue *)
type state

(* Gets the relevant information required *)
val get_town_center : color -> building_data
val getUnitId : unit_id -> unit_info option
val getTeamUnits : color -> unit_data list
val getBuildingId : building_id -> building_data option
val getBuildingTile : tile -> building_data option
val getTeamBuildings : color -> building_data list
val getResourceTile : tile -> resource_data option
val inRange : unit_id -> attackable_object -> bool


(* Initiates all states to primitive values *)
(* Gui interaction here *)
val initState : unit -> state

(* Initiates towncenter and villagers *)
(* Gui interaction here *)
val initObjects : unit -> unit

val check_tile : tile -> bool

(* The following sets of functions adds the respective actions to the queue *)
val addBuild : unit_id -> unit
val addAttack : unit_id -> attackable_object -> unit
val addMove : unit_id -> vector -> unit
val clearAttack : unit_id -> unit
val clearMove : unit_id -> unit
(* Gui interaction here *)
val addSpawn : state -> building_id -> unit_type -> unit
(* Gui interaction here *)
val addCollect : state -> unit_id -> unit


(* Performs the appropriate upgrades *)
(* Gui interaction here *)
val upgradeAge : state -> color -> unit
(* Gui interaction here *)
val upgradeUnit : state -> color -> unit_type -> unit

(* Health checking happens here *)
(* Gui interaction here *)
val increment_time : state -> unit
