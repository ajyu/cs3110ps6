open Definitions
open Constants
open Util
open State
open Netgraphics

type game = state * Mutex.t

let initGame () : game = (initState (), Mutex.create ())

let initUnitsAndBuildings (s, m) : unit = (initObjects ())

let startGame g : unit = ()
  
let handleAction g act c : command = 
  let (s, m) = g in
  let (team_r, team_b, _) = !s in
  let (_,units_r,_,_,_,_,_) = team_r
  and (_,units_b,_,_,_,_,_) = team_b in
  Mutex.lock m;
  let res =
    let correct_color id colr : bool = 
      if colr = Red then (List.exists (fun (x,_,_,_) -> id = x) units_r)
      else (List.exists (fun (x,_,_,_) -> id = x) units_b) in
    match act with
			| QueueCollect unit_id -> 
        (* If the unit is not a Villager, or is not standing on a resource to *)
        (* be collected, then you should return Failed, otherwise handle the *)
        (* resource collection. This should also fail if the unit performing *)
        (* the collection is not a Villager *)
	      if (correct_color unit_id c) then
          (match (getUnitId unit_id) with
            | Some (_,u_type,_,pos) ->
              if u_type != Villager then Failed
              else 
                (match (getResourceTile (tile_of_pos pos)) with
                  | Some (r_tile,r_type,r_cnt) ->
                    if r_cnt > 0 then ((addCollect s unit_id); Success)
                    else Failed
                  | None -> Failed)
            | None -> Failed)
        else Failed
			| QueueMove(unit_id,pos) -> 
	      if (correct_color unit_id c)
	      then ((addMove unit_id pos); Success) else Failed 
	    | Talk str -> (Netgraphics.add_update(DisplayString(c, str)); Success)
			| QueueAttack (unit_id, attackable_object) -> 
        (* If the target is already dead, or is of the same color as the unit*)
        (* doing the attack, then you should return Failed, otherwise queue *)
        (* the attack. This should also fail if the unit queuing the attack *)
        (* is a Villager. *)
	      if (correct_color unit_id c) then
          match attackable_object with
            | Unit (a_id) -> 
		          (match (getUnitId unit_id),(getUnitColor a_id) with
		            | (Some (_,u_type,_,_)), (Some clr) ->
		              if (u_type = Villager) || (clr = c) then Failed
		              else ((addAttack unit_id attackable_object); Success)
		            | _ -> Failed)
            | _ -> Failed
        else Failed
			| QueueBuild (unit_id, building_type) -> 
        (* This should fail if the building will overlap with any type of *)
        (* resource (keep in mind that buildings occupy 2x2 tiles), or if *)
        (* the player does not have enough food or wood to add this building.*)
        (* This should also fail if the unit performing the build is not a *)
        (* Villager, or if the building to be added is a Town Center *)
	      if (correct_color unit_id c) then
          let (f,w) = (getTeamResources s c) in
          let (f_cost,w_cost) = cBARRACKS_COST in
          let enough_resources = (f >= f_cost) && (w >= w_cost) in
          if (building_type = TownCenter) || enough_resources then Failed else          
            (match (getUnitId unit_id) with
              | Some (_,u_type,_,pos) ->
                if (u_type = Villager) && (check_tile (tile_of_pos pos)) 
                then 
                  (addResourcesNoScore s c (-f_cost) (-w_cost);
                  addBuild unit_id;
                  Success)
                else Failed
              | None -> Failed)
        else Failed
			| QueueSpawn (building_id, unit_type) -> 
        (* This should fail if the player does not have enough food or wood *)
        (* to create this unit. This should also fail if the player is trying*)
        (* to spawn a unit without having done the necessary Upgrades, or if *)
        (* the player is trying to spawn a unit at the wrong building *)
	      if (correct_color building_id c) then
          let (f,w) = (getTeamResources s c) in
          let (f_pcost,w_pcost) = cSPAWN_PIKEMAN_COST 
          and (f_acost,w_acost) = cSPAWN_ARCHER_COST
          and (f_kcost,w_kcost) = cSPAWN_KNIGHT_COST
          and (f_vcost,w_vcost) = cSPAWN_VILLAGER_COST in
          let can_build_p = (f >= f_pcost) && (w >= w_pcost)
          and can_build_a = (f >= f_acost) && (w >= w_acost)
          and can_build_k = (f >= f_kcost) && (w >= w_kcost)
          and can_build_v = (f >= f_vcost) && (w >= w_vcost) in
	        (match (getBuildingId building_id) with
	          | Some (_,b_type,_,_) ->
                let check_helper (a,f,w,(pi,ar,kn)) =
				          (match unit_type with
				            | ElitePikeman | EliteArcher | EliteKnight | Archer | Knight ->
				              if a = DarkAge && b_type = TownCenter then Failed
				              else if pi && unit_type = ElitePikeman && can_build_p
				              then ((addSpawn s building_id unit_type); Success)
				              else if ar && unit_type = EliteArcher && can_build_a
				              then ((addSpawn s building_id unit_type); Success)
				              else if kn && unit_type = EliteKnight && can_build_k
				              then ((addSpawn s building_id unit_type); Success)
				              else if unit_type = Archer && can_build_a
		                  then 
                        (if ar 
                        then ((addSpawn s building_id EliteArcher); Success)
                        else ((addSpawn s building_id unit_type); Success))
		                  else if unit_type = Knight && can_build_k
		                  then 
                        (if kn
                        then ((addSpawn s building_id EliteKnight); Success)
                        else ((addSpawn s building_id unit_type); Success))
		                  else Failed
				            | Pikeman ->
				              if b_type = TownCenter then Failed
				              else if unit_type = Pikeman && can_build_p
				              then 
                        (if pi
                        then ((addSpawn s building_id ElitePikeman); Success)
                        else ((addSpawn s building_id unit_type); Success))
				              else Failed
				            | Villager ->
				              if (b_type = Barracks) && (not can_build_v) then Failed
				              else ((addSpawn s building_id unit_type); Success)) in
              (match c with 
                | Red ->
                  let (_,_,_,a,f,w,(pi,ar,kn)) = team_r in
                  check_helper (a,f,w,(pi,ar,kn))
                | Blue ->
                  let (_,_,_,a,f,w,(pi,ar,kn)) = team_b in
	                check_helper (a,f,w,(pi,ar,kn)))
	          | None -> Failed)
        else Failed
			| ClearAttack id -> 
	      if (correct_color id c) then
          (match (getUnitId id) with
            | Some (_,u_type,_,_) ->
              if u_type = Villager then Failed
              else ((clearAttack id); Success)
            | None -> Failed)
        else Failed
			| ClearMove id -> 
        if (correct_color id c) then
          (match (getUnitId id) with
            | Some x -> ((clearMove id); Success)
            | None -> Failed)
        else Failed
			| Upgrade upgrade_type -> 
        let (_,_,_,a1,fc1,wc1,u1),(_,_,_,a2,fc2,wc2,u2),_ = !s in
	      (match upgrade_type with
	        | AgeUpgrade -> let f,w = cUPGRADE_AGE_COST in
             (match c with
                Red  -> if fc1-f < 0 || wc1-w < 0 then Failed else
                  (match a1 with 
                     ImperialAge -> Failed
                   | DarkAge -> (upgradeAge s c) ; Success)
              | Blue -> if fc2-f < 0 || wc2-w < 0 then Failed else
                  (match a2 with
                     ImperialAge -> Failed
                   | DarkAge -> (upgradeAge s c) ; Success))
          | UnitUpgrade unit_type -> 
            let (pi1,ar1,kn1),(pi2,ar2,kn2) = u1,u2 in
            (match a1 with DarkAge -> Failed | ImperialAge ->
	            (match unit_type with
	              Villager -> Failed
	            | Knight | EliteKnight -> let f,w = cUPGRADE_KNIGHT_COST in
	              (match c with
	                Red  -> if kn1 || fc1-f < 0 || wc1-w < 0 then Failed
	                        else ((upgradeUnit s c EliteKnight); Success)
	               |Blue -> if kn2 || fc2-f < 0 || wc2-w < 0 then Failed
	                        else ((upgradeUnit s c EliteKnight); Success))
	            | Archer | EliteArcher -> let f,w = cUPGRADE_ARCHER_COST in
	              (match c with
	                Red  -> if ar1 || fc1-f < 0 || wc1-w < 0 then Failed
	                        else ((upgradeUnit s c EliteArcher); Success)
	               |Blue -> if ar2 || fc2-f < 0 || wc2-w < 0 then Failed 
	                        else ((upgradeUnit s c EliteArcher); Success))
	            | Pikeman | ElitePikeman -> let f,w = cUPGRADE_PIKEMAN_COST in 
	              (match c with
	                Red  -> if pi1 || fc1-f < 0 || wc1-w < 0 then Failed 
	                        else ((upgradeUnit s c ElitePikeman); Success)
	               |Blue -> if pi2 || fc2-f < 0 || wc2-w < 0 then Failed 
	                        else ((upgradeUnit s c ElitePikeman); Success)))))
		in
  Mutex.unlock m;
  Result res

let handleStatus g status : command = 
  let (s, m) = g in
  let (team_r, team_b, t) = !s in
  Mutex.lock m;
  let data =
    match status with
			| TeamStatus c -> 
        (match c with
          | Red -> TeamData team_r
          | Blue -> TeamData team_b)
			| UnitStatus id -> 
        (match (getUnitId id) with
          | Some u -> UnitData u
          | None -> failwith "TODO: what to do when no unit")
			| BuildingStatus id -> 
        (match (getBuildingId id) with
          | Some b -> BuildingData b
          | None -> failwith "TODO: what to do when no building")
			| GameStatus -> GameData !s
			| ResourceStatus -> 
	      let mk_rlist = fun acc el -> 
          (match (getResourceTile el) with
		        | Some (r_tile,r_type,r_count) -> (el,r_type,r_count)::acc
		        | None -> acc) in
        let food_status = List.fold_left mk_rlist [] cFOOD_TILES in
        ResourceData (List.fold_left mk_rlist food_status cWOOD_TILES)
    in
  Mutex.unlock m;
  Data data

let check_for_game_over s curr_time : game_result option =
	(* check timeout *)
  let (team_r, team_b, start_time) = !s in
  let (score_r,unit_lst_r,build_lst_r,_,food_r,wood_r,_) = team_r
  and (score_b,unit_lst_b,build_lst_b,_,food_b,wood_b,_) = team_b in
  let (_,_,tc_h_r,_) = (get_town_center Red)
  and (_,_,tc_h_b,_) = (get_town_center Blue) in
  if ((curr_time -. start_time) < cTIME_LIMIT)
  
  then (* regular checks *)
    (* team with 0 units loses *)
    let units_r = List.length (getTeamUnits Red)
    and units_b = List.length (getTeamUnits Blue) in
    if (units_r = 0 && units_b = 0) then Some Tie
    else if units_r = 0 then Some (Winner Blue)
    else if units_b = 0 then Some (Winner Red)
    else
	  (* team without town center loses *)
	  if tc_h_r = 0 && tc_h_b = 0 then Some Tie
	  else if tc_h_r = 0 then Some (Winner Blue) 
    else if tc_h_b = 0 then Some (Winner Red)
    (* game continues as usual *)
    else None
    
  else (* time out checks *)
	  (* highest score wins *)
	  if score_r > score_b then Some (Winner Red) 
    else if score_b > score_r then Some (Winner Blue)
	  else
	  (* most resources wins *)
    let res_r = food_r + wood_r and res_b = food_b + wood_b in 
    if res_r > res_b then Some (Winner Red) 
    else if res_b > res_r then Some (Winner Blue)
    else 
	  (* most objects (buildings+units) wins *)
	  let obj_r = 
      (List.length (getTeamUnits Red))+(List.length (getTeamBuildings Red))
	  and obj_b = 
      (List.length (getTeamUnits Blue))+(List.length (getTeamBuildings Blue)) in
	  if obj_r > obj_b then Some (Winner Red) 
    else if obj_b > obj_r then Some (Winner Blue)
	  else
	  (* most health in town center wins *)
	  if tc_h_r > tc_h_b then Some (Winner Red) 
    else if tc_h_b > tc_h_r then Some (Winner Blue) 
    else Some Tie
    
let handleTime g new_time : game_result option = 
  let (s, m) = g in
  Mutex.lock m;
  let res = check_for_game_over s new_time in
  (match res with
   | Some c -> ()
   | None -> (increment_time s));
  Mutex.unlock m;
  res
