open Definitions
open Util
open Constants
open Netgraphics

(* There exists one of each of the following for each unit *)
(* attacks should be empty at the end of each timestep *)
type attacks = attackable_object Queue.t
type moves = position Queue.t

(* There exists one of each of the following for each game *)
(* Consider calclulating the max number of cells to create in the hashtable *)
(* The timer is the time at which the unit can start to do something *)
let units : (unit_id,(unit_data*moves*attacks*color*timer)) Hashtbl.t =
  Hashtbl.create 100
let resources : (tile,resource_data) Hashtbl.t = Hashtbl.create 100
let buildings : (building_id,building_data*color) Hashtbl.t = Hashtbl.create 100
let building_tiles : (tile,building_id) Hashtbl.t = Hashtbl.create 100
let in_progress: (unit_id,building_data) Hashtbl.t = Hashtbl.create 100
(* The following should be empty following each increment_time *)
let builds : unit_id Queue.t = Queue.create ()

(* First team in game_data is red, second blue *)
(* The unit_data list in team_data is NOT used!!! *)
type state = game_data ref

(* Red, blue *)
let spawned : (bool*bool) ref = ref (false,false)
let town_center : (building_data option*building_data option) ref =
  ref (None, None)
let get_town_center color : building_data =
  let r,b = !town_center in
    match color with
      Red -> (match r with
        Some(bd) -> bd
      | None -> (0, TownCenter, 0, (0,0)) )
    | Blue -> (match b with
       Some(bd) -> bd
      | None -> (0, TownCenter, 0, (0,0)) )

(* first item is food, second is wood *)
let getTeamResources state color : (resource_count * resource_count) =
  let (red_t,blue_t,_) = !state in
  match color with
    | Red -> let (_,_,_,_,f,w,_) = red_t in (f,w)
    | Blue -> let (_,_,_,_,f,w,_) = blue_t in (f,w)
let getUnitColor unit_id : color option =
  try let (_,_,_,c,_) = Hashtbl.find units unit_id in
    Some (c)
  with _ -> None
let getUnitId unit_id : unit_data option =
  try let (udat,_,_,_,_) = Hashtbl.find units unit_id in
    Some(udat)
  with _ -> None
let getTeamUnits color : unit_data list =
  let filterUnit uid ((_,ut,h,pos),_,_,col,_) lst =
    match color,col with
      Red,Red | Blue,Blue -> (uid,ut,h,pos)::lst
    | _ -> lst in
  Hashtbl.fold filterUnit units []
let getBuildingId building_id : building_data option =
  try let (bdat,_) = Hashtbl.find buildings building_id in
    Some(bdat)
  with _ -> None
(* Get a list of all buildings of a team *)              
let getTeamBuildings color : building_data list =
  let filterBuilding bid ((_,bt,h,t),col) lst =
    match color,col with
      Red,Red | Blue,Blue -> (bid,bt,h,t)::lst
    | _ -> lst in
    Hashtbl.fold filterBuilding buildings []
let getBuildingTile tile : building_data option =
  try
    let bID = Hashtbl.find building_tiles tile in
    let (bdat,_) = Hashtbl.find buildings bID in
      Some(bdat)
  with _ -> None
let getResourceTile tile : resource_data option =
  try let res_dat = Hashtbl.find resources tile in
    Some (res_dat)
  with _ -> None
let in_range unit_id attackable_object : bool =
  (* get unit_type, see what its range is *)
  match (getUnitId unit_id) with
    | Some (_,u_type,_,pos) -> 
      (* find range of the attacking unit *)
      let range = length_of_range (get_unit_type_range u_type) in
      let obj_pos = ref (-1.0,-1.0) in
      (match attackable_object with
        | Building b_id ->
          (match (getBuildingId b_id) with
            | Some (_,_,_,obj_tile) -> obj_pos := (position_of_tile obj_tile)
            | None -> obj_pos := (-1.0,-1.0)) 
        | Unit u_id ->
          (match getUnitId u_id with
            | Some (_,_,_,obj_u_pos) -> obj_pos := obj_u_pos
            | None -> obj_pos := (-1.0,-1.0)));
      (range >= (distance pos !obj_pos))
    | None -> false

(* Helper functions *)
let addResources state color f w : unit =
  let (t1,t2,_) = !state in
  let (s1,udl1,bdl1,a1,fc1,wc1,u1),(s2,udl2,bdl2,a2,fc2,wc2,u2),t = !state in
    match color with
      Red  -> state := (s1+f+w,udl1,bdl1,a1,fc1+f,wc1+w,u1),t2,t ;
              add_update (DoCollect(0,color,Food,f)) ;
              add_update (DoCollect(0,color,Wood,w)) ;
              add_update (UpdateScore(color,s1+f+w))
    | Blue -> state := t1,(s2+f+w,udl2,bdl2,a2,fc2+f,wc2+w,u2),t ;
              add_update (DoCollect(0,color,Food,f)) ;
              add_update (DoCollect(0,color,Wood,w)) ;
              add_update (UpdateScore(color,s2+f+w))
let addResourcesNoScore state color f w : unit =
  let (t1,t2,_) = !state in
  let (s1,udl1,bdl1,a1,fc1,wc1,u1),(s2,udl2,bdl2,a2,fc2,wc2,u2),t = !state in
    match color with
      Red  -> state := (s1,udl1,bdl1,a1,fc1+f,wc1+w,u1),t2,t;
              add_update (DoCollect(0,color,Food,f)) ;
              add_update (DoCollect(0,color,Wood,w))
    | Blue -> state := t1,(s2,udl2,bdl2,a2,fc2+f,wc2+w,u2),t ;
              add_update (DoCollect(0,color,Food,f)) ;
              add_update (DoCollect(0,color,Wood,w))
let addScore state color x =
  let (t1,t2,_) = !state in
  let (s1,udl1,bdl1,a1,fc1,wc1,u1),(s2,udl2,bdl2,a2,fc2,wc2,u2),t = !state in
    match color with
      Red  -> state := (s1+x,udl1,bdl1,a1,fc1,wc1,u1),t2,t ;
              add_update (UpdateScore(color,s1+x))
    | Blue -> state := t1,(s2+x,udl2,bdl2,a2,fc2,wc2,u2),t ;
              add_update (UpdateScore(color,s2+x))
(* helper function to check if this tile is blocked by something *)
(* returns true if the tile is clear, false if there is a resource *)
(* or building blocking it *)
let check_tile tile : bool = 
  let r,c = tile in
  if (r < 0 || r > cNUM_Y_TILES-1) && (c < 0 || c > cNUM_X_TILES-1) then false
  else 
	  let r = r-1 and c = c-1 in
	  let blocked = ref true in
	  for i = 0 to 2 do
	    for j = 0 to 2 do
			  let bt = (getBuildingTile ((r+i),(c+j))) 
	      and rt = (getResourceTile ((r+i),(c+j))) in
	      blocked := !blocked && (rt = None && bt = None)
	    done
	  done;
	  not !blocked

(* Initiates resources to start values *)  
let initResources () : unit =
  let update_food = fun el ->
    Hashtbl.replace resources el (el,Food,cINITIAL_FOOD);
    add_update (UpdateResource(el,cINITIAL_FOOD)) in
  let update_wood = fun el ->
    Hashtbl.replace resources el (el,Wood,cINITIAL_WOOD);
    add_update (UpdateResource(el,cINITIAL_WOOD)) in
  (List.iter update_food cFOOD_TILES);
  (List.iter update_wood cWOOD_TILES)

(* Initiates all states to primitive values *)
let initState () : state =
  send_update InitGraphics; 
  send_update (InitFood(cFOOD_TILES));
  send_update (InitWood(cWOOD_TILES));
  let reset_everything =
    Hashtbl.clear units;
    Hashtbl.clear resources;
    Hashtbl.clear buildings;
    Hashtbl.clear building_tiles;
    Hashtbl.clear in_progress;
    Queue.clear builds;
    town_center := (None,None);
    spawned := (false,false) in
  let g_data : game_data = 
    let default_team : team_data = 
      let s : score = 0 in
      let u_data : unit_data list = [] in
      let b_data : building_data list = [] in
      let a : age = DarkAge in
      let f_count : food_count = 0 in
      let w_count : wood_count = 0 in
      let u : upgrades = (false, false, false) in
      (s, u_data, b_data, a, f_count, w_count, u) in
    (default_team,default_team,(Unix.time ())) in
  initResources ();
  reset_everything;
  (ref g_data)

(* The following sets of functions adds the respective actions to the queue *)
(* Attacks are added without regard to range *)
let addBuild unit_id : unit =
  if Hashtbl.mem units unit_id then Queue.push unit_id builds
let addAttack unit_id att_obj : unit =
  try
    let (_,_,att,_,_) = Hashtbl.find units unit_id in
      Queue.push att_obj att 
  with _ -> ()
(* Adds intermediate moves to queue *)
let addMove unit_id (nx,ny) : unit =
  try
    let ((_,utype,_,_),mov,_,_,_) = Hashtbl.find units unit_id in
    let (px,py) = Queue.peek mov in
    let rec queueMove (currx,curry) =
      let unscaled = (nx-.currx , ny-.curry) in
      let usx,usy = unscaled in
      let unscaledLength = length unscaled in
      let ux,uy = normalize unscaled in
      let speed = get_unit_type_speed utype in
      let scaled = (ux*.speed, uy*.speed) in
      let sx, sy = scaled in
      let sfx,sfy = sx+.currx , sy+.curry in
      let scaledLength = length scaled in
        if scaledLength > unscaledLength
        then Queue.push (usx+.currx,usy+.curry) mov
        else (Queue.push (sfx, sfy) mov ; 
              queueMove (sfx,sfy)) in
      queueMove (px,py)
  with _ -> ()
let clearAttack unit_id : unit =
  try
    let (_,_,att,_,_) = Hashtbl.find units unit_id in
      Queue.clear att
  with _ -> ()
let clearMove unit_id : unit =
  try
    let (_,mov,_,_,_) = Hashtbl.find units unit_id in
      Queue.clear mov
  with _ -> ()



(* The following happen IMMEDIATELY and is NOT part of increment_time*)
(* Assumes that spawns have already been checked for appropriate building
 * type in game *)
let addSpawn state building_id unit_type : unit =
  try
  let ((_,_,_,tile),col) = Hashtbl.find buildings building_id in
  let pos = position_of_tile tile in
  let unit_id = next_available_id () in
  let h = get_unit_type_health unit_type in
  let mov : position Queue.t = Queue.create () in
  let att : attackable_object Queue.t  = Queue.create () in
  let f,w = match unit_type with
      Pikeman | ElitePikeman -> cSPAWN_PIKEMAN_COST
    | Archer | EliteArcher -> cSPAWN_ARCHER_COST
    | Knight | EliteKnight -> cSPAWN_KNIGHT_COST 
    | Villager -> cSPAWN_VILLAGER_COST in
  match col,(!spawned) with
    Red,(false,b_s)  -> (spawned := (true,b_s) ;
      Hashtbl.replace units unit_id ((unit_id,unit_type,h,pos),mov,att,col,0.);
      add_update (AddUnit(unit_id,unit_type,pos,h,col)) ;
      addResourcesNoScore state col f w)
  | Blue,(r_s,false) -> (spawned := (r_s,true) ;
      Hashtbl.replace units unit_id ((unit_id,unit_type,h,pos),mov,att,col,0.);
      add_update (AddUnit(unit_id,unit_type,pos,h,col)) ;
      addResourcesNoScore state col f w )
  | _ -> ();
  let rlist = getTeamUnits Red
  and blist = getTeamUnits Blue in
  let (r1,_,r3,r4,r5,r6,r7),(b1,_,b3,b4,b5,b6,b7),t = !state in
  state := ((r1,rlist,r3,r4,r5,r6,r7),(b1,blist,b3,b4,b5,b6,b7),t)
  with _ -> ()
(*Queue.push (col,unit_type,pos) spawns*)
(* To process a collect:
 * 1. Make sure the the unit isn't under a cooldown time!
 * 2. Decrement the amount of resources on that tile
 * 3. Increment the resources the team has
 * 4. Process the new cooldown time for the unit
 *)
let addCollect state unit_id : unit =
  try
  let (_,_,_,a1,_,_,_),(_,_,_,a2,_,_,_),_ = !state in
  let ((_,ut,h,pos),mov,att,col,time) = Hashtbl.find units unit_id in
  let tile = tile_of_pos pos in
  let (rID,rt,rc) = Hashtbl.find resources tile in
  let r_c = cRESOURCE_COLLECTED in
  let ar_c = cADVANCED_RESOURCE_COLLECTED in
  let curr_time = Unix.time () in
    if (curr_time > time) then
      let repl ammt = if rc-ammt>=0 
        then (Hashtbl.replace resources rID (rID,rt,rc-ammt); 
             Hashtbl.replace units unit_id
               ((unit_id,ut,h,pos),mov,att,col,curr_time);
             true) else false in
      match col,rt,a1,a2 with
        Red,Food,DarkAge,_      -> if repl r_c then 
            (addResources state Red r_c 0 ;
             add_update (DoCollect(unit_id,col,rt,r_c)) ;
             add_update (UpdateResource(tile, r_c)))
      | Red,Food,ImperialAge,_  -> if repl ar_c then 
             (addResources state Red ar_c 0 ;
             add_update (DoCollect(unit_id,col,rt,ar_c)) ;
             add_update (UpdateResource(tile, ar_c)))
      | Red,Wood,DarkAge,_      -> if repl r_c then 
            (addResources state Red 0 r_c ;
             add_update (DoCollect(unit_id,col,rt,r_c)) ;
             add_update (UpdateResource(tile, r_c)))
      | Red,Wood,ImperialAge,_  -> if repl ar_c then 
             (addResources state Red 0 ar_c ;
             add_update (DoCollect(unit_id,col,rt,ar_c)) ;
             add_update (UpdateResource(tile, ar_c)))
      | Blue,Food,_,DarkAge     -> if repl r_c then 
             (addResources state Blue r_c 0 ;
             add_update (DoCollect(unit_id,col,rt,r_c)) ;
             add_update (UpdateResource(tile, r_c)))
      | Blue,Food,_,ImperialAge -> if repl ar_c then 
             (addResources state Blue ar_c 0 ;
             add_update (DoCollect(unit_id,col,rt,ar_c)) ;
             add_update (UpdateResource(tile, ar_c)))
      | Blue,Wood,_,DarkAge     -> if repl r_c then 
             (addResources state Blue 0 r_c ;
             add_update (DoCollect(unit_id,col,rt,r_c)) ;
             add_update (UpdateResource(tile, r_c)))
      | Blue,Wood,_,ImperialAge -> if repl ar_c then 
             (addResources state Blue 0 ar_c ;
             add_update (DoCollect(unit_id,col,rt,ar_c)) ;
             add_update (UpdateResource(tile, ar_c)))
  with _ -> ()

(* Upgrades need to be checked in game before these functions are called *)
let upgradeAge state color : unit =
  let (t1,t2,_) = !state in
  let (s1,udl1,bdl1,a1,fc1,wc1,u1),(s2,udl2,bdl2,a2,fc2,wc2,u2),t = !state in
  let f,w = cUPGRADE_AGE_COST in
    match color with
      Red  -> state := (s1-f-w,udl1,bdl1,ImperialAge,fc1-f,wc1-w,u1),t2,t ;
              add_update (DoCollect(0,color,Food,-f)) ;
              add_update (DoCollect(0,color,Wood,-w)) ;
              add_update (UpgradeAge(color)) ;
              add_update (UpdateScore(color,s1-f-w))
    | Blue -> state := t1,(s2-f-w,udl2,bdl2,ImperialAge,fc2-f,wc2-w,u2),t ;
              add_update (DoCollect(0,color,Food,-f)) ;
              add_update (DoCollect(0,color,Wood,-w)) ;
              add_update (UpgradeAge(color)) ;
              add_update (UpdateScore(color,s2-f-w))

let upgradeUnit state color unit_type : unit =
  let (t1,t2,_) = !state in
  let (s1,udl1,bdl1,a1,fc1,wc1,u1),(s2,udl2,bdl2,a2,fc2,wc2,u2),t = !state in
  let (pi1,ar1,kn1),(pi2,ar2,kn2) = u1,u2 in
  let changeUnit col uid ((_,ut,h,pos),mv,at,co,ti) =
    match col,co with 
      Red,Red -> (match unit_type,ut with
          EliteKnight,Knight | EliteArcher,Archer | ElitePikeman,Pikeman -> 
            Hashtbl.replace units uid ((uid,unit_type,h,pos),mv,at,co,ti) ;
            add_update (UpgradeUnit(unit_type,col))
        | _ -> ())
    | Blue, Blue -> (match unit_type,ut with
          EliteKnight,Knight | EliteArcher,Archer | ElitePikeman,Pikeman -> 
            Hashtbl.replace units uid ((uid,unit_type,h,pos),mv,at,co,ti) ;
            add_update (UpgradeUnit(unit_type,col))
        | _ -> ())
    | _ -> () in
    Hashtbl.iter (changeUnit color) units;
    match color with
      Red -> (match unit_type with
                EliteKnight -> let f,w = cUPGRADE_KNIGHT_COST in
                  state := (s1-f-w,udl1,bdl1,a1,fc1-f,wc1-w,(pi1,ar1,true)),t2,t;
                  add_update (DoCollect(0,color,Food,-f)) ;
                  add_update (DoCollect(0,color,Wood,-w)) ;
                  add_update (UpdateScore(color,s1-f-w))
              | EliteArcher -> let f,w = cUPGRADE_ARCHER_COST in
                  state := (s1-f-w,udl1,bdl1,a1,fc1-f,wc1-w,(pi1,true,kn1)),t2,t;
                  add_update (DoCollect(0,color,Food,-f)) ;
                  add_update (DoCollect(0,color,Wood,-w)) ;
                  add_update (UpdateScore(color,s1-f-w))
              | ElitePikeman -> let f,w = cUPGRADE_PIKEMAN_COST in
                  state := (s1-f-w,udl1,bdl1,a1,fc1-f,wc1-w,(true,ar1,kn1)),t2,t;
                  add_update (DoCollect(0,color,Food,-f)) ;
                  add_update (DoCollect(0,color,Wood,-w)) ;
                  add_update (UpdateScore(color,s1-f-w))
              | _ -> () )
    | Blue -> (match unit_type with
                EliteKnight -> let f,w = cUPGRADE_KNIGHT_COST in
                  state := t1,(s2-f-w,udl2,bdl2,a2,fc2-f,wc2-w,(pi2,ar2,true)),t;
                  add_update (DoCollect(0,color,Food,-f)) ;
                  add_update (DoCollect(0,color,Wood,-w)) ;
                  add_update (UpdateScore(color,s2-f-w))
              | EliteArcher -> let f,w = cUPGRADE_ARCHER_COST in
                  state := t1,(s2-f-w,udl2,bdl2,a2,fc2-f,wc2-w,(pi2,true,kn2)),t;
                  add_update (DoCollect(0,color,Food,-f)) ;
                  add_update (DoCollect(0,color,Wood,-w)) ;
                  add_update (UpdateScore(color,s2-f-w))
              | ElitePikeman -> let f,w = cUPGRADE_PIKEMAN_COST in
                  state := t1,(s2-f-w,udl2,bdl2,a2,fc2-f,wc2-w,(true,ar2,kn2)),t;
                  add_update (DoCollect(0,color,Food,-f)) ;
                  add_update (DoCollect(0,color,Wood,-w)) ;
                  add_update (UpdateScore(color,s2-f-w))
              | _ -> () )

(* Handling attacks:
 * 1. Go down queue until empty, only carrying out attacks 
 *    a) within range
 *    b) without cooldown time
 * 2. For legal attacks, decrement opponent health
 * 3. Increase team score
 * 4. Add cooldown time
 * 5. Empty queue
 *)
let handleAttacks state curr_time : unit =
  let handler atker ((_,ut,h,p),mov,att,col,t) =
    if ut = Villager then () else
	    let range = get_unit_type_range ut in
	    let baseattk = get_unit_type_attack_damage ut in
	    let rec queuehandler b =
	      if b || (Queue.is_empty att) then () else
	      match Queue.pop att with 
	        Building(bid) ->
	          let ((_,bt,h,(x,y)),col) = Hashtbl.find buildings bid in
	          let dist1 = distance p (position_of_tile (x,y)) in
	          let dist2 = distance p (position_of_tile (x+1,y)) in
	          let dist3 = distance p (position_of_tile (x,y+1)) in
	          let dist4 = distance p (position_of_tile (x+1,y+1)) in
	            if (dist1 < range || dist2 < range || dist3 < range || dist4 < range)
	               && t+.get_unit_type_cooldown ut < curr_time then (
	              Hashtbl.replace buildings bid ((bid,bt,h-baseattk,(x,y)),col) ;
	              add_update (DoAttack(atker)) ;
	              add_update (UpdateBuilding(bid,(h-baseattk))) ;
	              Hashtbl.replace units atker ((atker,ut,h,p),mov,att,col,curr_time);
	              addScore state col baseattk )
	      | Unit(atkee) ->
	        let ((_,ut2,h2,p2),m2,a2,c2,t2) = Hashtbl.find units atkee in
	        let dmg = match ut with
	          Archer | EliteArcher -> (match ut2 with
	              Pikeman | ElitePikeman -> baseattk +cSTAB_BONUS
	            | _ -> baseattk )
	        | Knight | EliteKnight -> (match ut2 with
	              Archer | EliteArcher -> baseattk +cSTAB_BONUS
	            | _ -> baseattk )
	        | Pikeman | ElitePikeman -> (match ut2 with
	              Knight | EliteKnight -> baseattk +cSTAB_BONUS
	            | _ -> baseattk )
	        | _ -> 0 in
	        let dist = distance p p2 in
	          if dist < range && t+.get_unit_type_cooldown ut < curr_time then (
	            Hashtbl.replace units atkee ((atkee,ut2,h2-dmg,p2),m2,a2,c2,t2);
	            add_update (DoAttack(atker)) ;
	            add_update (UpdateUnit(atkee,(h2-dmg))) ;
	            Hashtbl.replace units atker ((atker,ut,h,p),mov,att,col,curr_time);
	            addScore state col dmg ) in
	      queuehandler false; Queue.clear att in
    Hashtbl.iter handler units

(* Handles dead units, buildings, and resources *)
let handleDead state curr_time : unit =
(* When a building cannot be completed due to the removal of a villager, it is
 * 1. Removed from in_progress 
 * 2. Removed from buildings
 * 3. Removed from building_tiles*)
  let handleUnits unit_id ((_,ut,h,p),mov,att,col,t) =
    if h <= 0 then (
      Hashtbl.remove units unit_id ; 
      try
        let (bid,bt,h,t) = Hashtbl.find in_progress unit_id in
          Hashtbl.remove buildings bid ; 
          Hashtbl.remove building_tiles t ;
          add_update (RemoveUnit(unit_id))
      with _ -> () ;
      Hashtbl.remove in_progress unit_id ;
      addScore state col cKILL_UNIT_SCORE ) in
  let handleBuildings building_id ((_,bt,h,t),col) =
    if h<= 0 then (
      Hashtbl.remove buildings building_id ;
      add_update (RemoveBuilding(building_id)) ;
      Hashtbl.remove building_tiles t ;
      addScore state col cKILL_BUILDING_SCORE ) in
  let handleResources tile (_,rt,rc) =
    if rc <= 0 then 
      (Hashtbl.remove resources tile;
      add_update (RemoveResource(tile))) in
  Hashtbl.iter handleUnits units ;
  Hashtbl.iter handleBuildings buildings ;
  Hashtbl.iter handleResources resources ;
  let rulist = getTeamUnits Red
  and bulist = getTeamUnits Blue in
  let rblist = getTeamBuildings Red
  and bblist = getTeamBuildings Blue in
  let (r1,_,_,r4,r5,r6,r7),(b1,_,_,b4,b5,b6,b7),t = !state in
  state := ((r1,rulist,rblist,r4,r5,r6,r7),(b1,bulist,bblist,b4,b5,b6,b7),t)

let handleCompleteBuild curr_time : unit =
  let handler unit_id (bid,bt,h,tile) = 
    try
      let ((_,_,_,_),_,_,col,t) = Hashtbl.find units unit_id in
        if (t+.cVILLAGER_COOLDOWN)>=curr_time then
          (Hashtbl.remove in_progress unit_id ;
          add_update (AddBuilding(bid,bt,tile,h,col)))
    with _ -> () in
    Hashtbl.iter handler in_progress

(* When a building is started, it is
 * 0a. Checked for adequate resources
 * 0b. Villager checked for cooldown time
 * 0c. Checked for building location again
 * 1. Removed from builds
 * 2. Added to in_progress
 * 3. Added to buildings (given a building_id & building_data)
 * 4. Building_tiles is updated with the connection from tile to building_id
 *)
let handleAddBuild state curr_time : unit = 
  let (s1,udl1,bdl1,a1,fc1,wc1,u1),(s2,udl2,bdl2,a2,fc2,wc2,u2),t = !state in
  let f,w = cBARRACKS_COST in
  let handler unit_id =
    try
      let ((_,a,b,pos),c,d,col,t) = Hashtbl.find units unit_id in
      let tile = tile_of_pos pos in
      if t+.cVILLAGER_COOLDOWN<=curr_time || (check_tile tile) then
        let bid = next_available_id () in
        let newbuild = ((bid,Barracks,cBARRACKS_HEALTH,tile)) in
        match col with
          Red -> if fc1-f >= 0 || wc1-w >= 0 then (
            Hashtbl.replace in_progress unit_id newbuild ;
            Hashtbl.replace building_tiles tile bid ;
            Hashtbl.replace buildings bid (newbuild,col) ;
            Hashtbl.replace units unit_id ((unit_id,a,b,pos),c,d,col,curr_time);
            add_update (DoBuild (tile,col)) ;
            addResourcesNoScore state col (-f) (-w))
        | Blue -> if fc2-f >= 0 || wc2-w >= 0 then (
            Hashtbl.replace in_progress unit_id newbuild ;
            Hashtbl.replace building_tiles tile bid ;
            Hashtbl.replace buildings bid (newbuild,col) ;
            Hashtbl.replace units unit_id ((unit_id,a,b,pos),c,d,col,curr_time) ;
            add_update (DoBuild (tile,col)) ;
            addResourcesNoScore state col (-f) (-w))
    with _ -> () in
    Queue.iter handler builds ;
    Queue.clear builds ;
    let rulist = getTeamUnits Red
	  and bulist = getTeamUnits Blue in
	  let rblist = getTeamBuildings Red
	  and bblist = getTeamBuildings Blue in
	  let (r1,_,_,r4,r5,r6,r7),(b1,_,_,b4,b5,b6,b7),t = !state in
	  state := ((r1,rulist,rblist,r4,r5,r6,r7),(b1,bulist,bblist,b4,b5,b6,b7),t)
  
(* Check that if villager && villager is building, then it shouldn't move*)
let handleMove curr_time : unit = 
  let handler unit_id ((_,ut,h,p),mov,att,col,t) =
    if not (Queue.is_empty mov) then
    match ut with
      Villager -> if not (Hashtbl.mem in_progress unit_id) then
      let nextmove = Queue.pop mov in
        Hashtbl.replace units unit_id ((unit_id,ut,h,nextmove),mov,att,col,t) ;
        add_update (MoveUnit (unit_id,[nextmove],col))
    | _ ->
      let nextmove = Queue.pop mov in
        Hashtbl.replace units unit_id ((unit_id,ut,h,nextmove),mov,att,col,t) ;
        add_update (MoveUnit (unit_id,[nextmove],col)) in
    Hashtbl.iter handler units

(*let handleResources : unit =                    *)
(*  let check_resources = fun k el ->             *)
(*    let (r_tile,r_type,r_cnt) = el in           *)
(*    add_update (UpdateResource(r_tile,r_cnt)) in*)
(*  Hashtbl.iter check_resources resources        *)

(* Health checking happens here *)
(* spawned has to be reset to false, false *)
let increment_time state : unit =
  let curr_time = Unix.time () in
    handleAttacks state curr_time;
    handleDead state curr_time;
    handleCompleteBuild curr_time;
    handleAddBuild state curr_time;
    handleMove curr_time;
    spawned := (false,false)




(********************************************************************)
(* Initiates towncenter and villagers *)
let initObjects () : unit =
  (* add towncenters *)
  (* get the red towncenter set up *)
  let get_tc_r () : tile =
	  let tc_x_r = ref (Random.int (cNUM_X_TILES/2))
	  and tc_y_r = ref (Random.int (cNUM_Y_TILES)) in
	  while check_tile (!tc_x_r,!tc_y_r) do
	      tc_x_r := Random.int (cNUM_X_TILES/2);
	      tc_y_r := Random.int (cNUM_Y_TILES)
	  done;
	  (!tc_y_r,!tc_x_r) in
  (* get the blue towncenter set up *)
  let get_tc_b () : tile =
	  let corr = 1 + cNUM_X_TILES/2 in
	  let tc_x_b = ref (corr + (Random.int (cNUM_X_TILES/2)))
	  and tc_y_b = ref (Random.int (cNUM_Y_TILES)) in
	  while check_tile (!tc_x_b,!tc_y_b) do
	      tc_x_b := corr + (Random.int (cNUM_X_TILES/2));
	      tc_y_b := Random.int (cNUM_Y_TILES)
	  done;
	  (!tc_y_b,!tc_x_b) in
  (* push onto building hashtables *)
  let bId_r : int = next_available_id () in
  let bId_b : int = next_available_id () in 
  let bTile_r : tile = get_tc_r () in
  let bTile_b : tile = get_tc_b () in
  town_center := (Some (bId_r,TownCenter,cTOWNCENTER_HEALTH,bTile_r),
                  Some (bId_b,TownCenter,cTOWNCENTER_HEALTH,bTile_b));
  Hashtbl.replace buildings bId_r ((bId_r,TownCenter,cTOWNCENTER_HEALTH,bTile_r),Red);
  Hashtbl.replace buildings bId_b ((bId_b,TownCenter,cTOWNCENTER_HEALTH,bTile_b),Blue);
  Hashtbl.replace building_tiles bTile_r bId_r;
  Hashtbl.replace building_tiles bTile_b bId_b;
  (* add to board *)
  add_update (AddBuilding(bId_r,TownCenter,bTile_r,cTOWNCENTER_HEALTH,Red));
  add_update (AddBuilding(bId_b,TownCenter,bTile_b,cTOWNCENTER_HEALTH,Blue));
  
  (* add villagers *)
  let add_start_villagers_r () : unit =
	  let (row_r, col_r) = position_of_tile bTile_r in
	  (* assume 12 is the max number of villagers *)
	  (* place 3 rows of 4 villagers *)
	  for i = 0 to 2 do
	    for j = 1 to 4 do
	      if (i*4 + j) > cSTARTING_VILLAGER_COUNT then ()
	      else
	        (* add villager to unit hashtable and board *)
	        let vId_r = next_available_id () in
	        let w_pos = row_r +. ((float_of_int i)+.1.)*.(cTILE_WIDTH/.4.) in
	        let h_pos = col_r +. (float_of_int j)*.(cTILE_WIDTH/.5.) in
	        let vPos_r = (w_pos,h_pos) in
	        let vData_r = (vId_r,Villager,cVILLAGER_HEALTH,vPos_r) in
	        Hashtbl.replace units vId_r
	          (vData_r,(Queue.create ()),(Queue.create ()),Red,(Unix.time ()));
	        add_update (AddUnit(vId_r,Villager,vPos_r,cVILLAGER_HEALTH,Red))
	    done
	  done in
  let add_start_villagers_b () : unit =
	  let (row_b, col_b) = position_of_tile bTile_b in
	  (* assume 12 is the max number of villagers *)
	  (* place 3 rows of 4 villagers *)
	  for i = 0 to 2 do
	    for j = 1 to 4 do
	      if (i*4 + j) > cSTARTING_VILLAGER_COUNT then ()
	      else
	        (* add villager to unit hashtable and board *)
	        let vId_b = next_available_id () in
	        let w_pos = row_b +. ((float_of_int i)+.1.)*.(cTILE_WIDTH/.4.) in
	        let h_pos = col_b +. (float_of_int j)*.(cTILE_WIDTH/.5.) in
	        let vPos_b = (w_pos,h_pos) in
	        let vData_b = (vId_b,Villager,cVILLAGER_HEALTH,vPos_b) in
	        Hashtbl.replace units vId_b
	          (vData_b,(Queue.create ()),(Queue.create ()),Blue,(Unix.time ()));
	        add_update (AddUnit(vId_b,Villager,vPos_b,cVILLAGER_HEALTH,Blue))
	    done
	  done in
  add_start_villagers_r ();
  add_start_villagers_b ()
    
