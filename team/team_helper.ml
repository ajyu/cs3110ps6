open Team
open Definitions
open Constants
open Util

(* statuses *)
let get_t_data c =
  let t_data = get_status (TeamStatus(c)) in
  (match t_data with
    | TeamData(d) -> d
    | _ -> failwith "Got the wrong data!")
let get_r_data () =
  let r_data = get_status ResourceStatus in
  (match r_data with
    | ResourceData(d) -> d
    | _ -> failwith "Got the wrong data!")

(* actions *)
let collect_action uid = 
	let res = send_action (QueueCollect(uid)) uid in
	(match res with
	  | Success -> print_endline "Collect Success!"
	  | _ -> print_endline "Collect Fail")
let clearMove_action uid =
	let res = send_action (ClearMove(uid)) uid in
	(match res with
	  | Success -> print_endline "ClearMove Success!"
	  | _ -> print_endline "ClearMove Fail")
let clearAttack_action uid =
	let res = send_action (ClearAttack(uid)) uid in
	(match res with
	  | Success -> print_endline "ClearAttack Success!"
	  | _ -> print_endline "ClearAttack Fail")
let move_action uid pos =   
	let res = send_action (QueueMove(uid, pos)) uid in
	(match res with
	  | Success -> print_endline "Move Success!"
	  | _ -> print_endline "Move Fail")
let build_action uid = 
  let res = send_action (QueueBuild(uid,Barracks)) uid in
  (match res with
	  | Success -> print_endline "BUILD SUCCESS OMG NOMNOMNOM!"
	  | _ -> print_endline "BUILD FAIL YOU'RE A POOP")
let spawn_action bid utype = 
  let res = send_action (QueueSpawn(bid,utype)) bid in
  (match res with
      | Success -> print_endline "Spawn Success!"
      | _ -> print_endline "Spawn Fail")
let attack_action uid aid = 
  let res = send_action (QueueAttack(uid,aid)) uid in
  (match res with
      | Success -> print_endline "Attack Success!"
      | _ -> print_endline "Attack Fail")

(* helper functions *)
let tile_has_resources r_tile r_lst : bool =
(*  List.exists (fun (tile,_,_) -> tile=r_tile) r_lst*)
  (List.exists (fun tile -> tile=r_tile) (cFOOD_TILES@cWOOD_TILES))

(*let tile_for_building_has_resources r_tile r_lst : bool =                *)
(*  let has_resources = ref false in                                       *)
(*	let (r,c) = r_tile in                                                  *)
(*	for i = r to (r+1) do                                                  *)
(*	  for j = c to (c+1) do                                                *)
(*	    has_resources := !has_resources || (tile_has_resources (i,j) r_lst)*)
(*	  done                                                                 *)
(*	done;                                                                  *)
(*	!has_resources                                                         *)
  
let tile_has_building b_tile b_lst : bool =
(*	let has_building = ref false in*)
  let on_tile = fun (_,_,_,tile) -> b_tile = tile in
(*    let (r,c) = tile in                                      *)
(*    for i = (r-1) to (r+1) do                                *)
(*      for j = (c-1) to (c+1) do                              *)
(*        if (is_valid_tile (i,j)) then                        *)
(*          (Printf.printf "checking %d,%d\n" i j;             *)
(*            has_building := !has_building ||                 *)
(*              (List.exists (fun (_,_,_,t) -> t=(i,j)) b_lst))*)
(*        else has_building := true                            *)
(*      done                                                   *)
(*    done;                                                    *)
(*    !has_building in                                         *)
  List.exists on_tile b_lst

let tile_is_blocked tile b_lst r_lst : bool =
(* true means the tile is blocked *) 
  (is_valid_tile tile) &&
    ((tile_has_resources tile r_lst) || (tile_has_building tile b_lst))

let get_new_barracks_location tile b_lst r_lst : tile =
	(* true if not blocked *)
	let check_blocked t : bool =
	  let (row,col) = t in
	  let is_not_blocked = ref true in
    for i = (row - 1) to (row + 1) do
      for j = (col - 1) to (col + 1) do
        is_not_blocked := (!is_not_blocked) && (not (tile_is_blocked (i,j) b_lst r_lst))
      done
    done;
	  !is_not_blocked in
	    
	let (r,c) = tile in
	let ntile = ref None in
	let count = ref 1 in
  while (!ntile = None) && (!count < (max cNUM_X_TILES cNUM_Y_TILES)) do
    for i = (r - !count) to (r + !count) do
      for j = (c - !count) to (c + !count) do
        if (check_blocked (i,j) && (!ntile = None)) then
          ntile := (Some(i,j))
        else ()
      done
    done;
    count := !count + 1
	done;  
     
	(match !ntile with
	  | Some t -> t
    | None -> tile)


let get_closest_villager tile c : unit_id * position =
  let (_,udl1,_,_,_,_,_) = get_t_data c in
  let (v_lst,_) = List.partition (fun (_,t,_,_) -> t=Villager) udl1 in
  let find_shortest_dist = 
    fun (min_uid,min_upos,min_dist) (uid,_,_,upos) -> 
      let t_pos = position_of_tile tile in
      let dist = distance t_pos upos in
        if (dist < min_dist) then 
          (uid,upos,dist) 
        else (min_uid,min_upos,min_dist) in
  let (id,pos,_) = List.fold_left find_shortest_dist 
    (0,(max_float,max_float),(max_float)) v_lst in
  (id,pos)

let get_closest_r base_pos : position * position =
  let (food_data,wood_data) = 
    List.partition (fun (_,t,_) -> t=Food) (get_r_data ()) in
  let helper = fun (min_dist,min_tile) (tile,_,_) ->
    let pos = position_of_tile tile in
    let dist = distance base_pos pos in
    if min_dist > dist then dist,pos else (min_dist,min_tile) in
  let (_,wood) = 
    List.fold_left helper (max_float,base_pos) wood_data in
  let (_,food) = 
    List.fold_left helper (max_float,base_pos) food_data in
  (wood, food)

let collect_resources uid (r_type: resource_type option) c: unit =
  let (_,udl,_,_,_,_,_) = get_t_data c in
  let rdl = get_r_data () in
  let my = List.find (fun (id,_,_,_)-> id=uid) udl in
  let (_,_,_,myPos) = my in
  let myTile = tile_of_pos myPos in
  let wood,food = get_closest_r myPos in
  let closest = if distance myPos wood < distance myPos food then
    wood else food in
  let isOnRes =
    try
      let _ = List.find (fun (rtile,_,_) -> rtile=myTile) rdl in
        true
    with _ -> false in
    if isOnRes then let _ =
        clearMove_action uid;
      send_action (QueueCollect(uid)) 0 in ()
    else match r_type with
      Some(rt) -> if rt=Wood then
        (
        clearMove_action uid;
        move_action uid wood
        )
      else move_action uid food
    | None -> 
        clearMove_action uid;
      move_action uid closest


    (*
let collect_resources uid (r_type: resource_type option) c : unit =
  (* check to make sure tile still has resources *)
  let v_pos : position = 
    let (_,u_data,_,_,_,_,_) = get_t_data c in
    let (_,_,_,pos) = List.find (fun (id,_,_,_) -> id=uid) u_data in pos in
  let v_tile : tile = tile_of_pos v_pos in
  let tile_has_resource_type r_type : bool =
    List.exists (fun (tile,t,_) -> (tile=v_tile && t=r_type)) (get_r_data ()) in
  let board_has_resource r_option : bool = 
    match r_option with
      | Some r -> List.exists (fun (_,t,_) -> t=r) (get_r_data ())
      | None -> not ([] = (get_r_data ())) in
  match r_type with
    | Some r ->
      (* if this tile has the specific resources *)
      if tile_has_resource_type r then
        (Printf.printf "COLLECTING action resource 1\n";
        collect_action uid)
      else if board_has_resource r_type then
        ( (* clearMove_action uid; *)
        let (closest_wood,closest_food) = get_closest_r v_pos in
        let closest_r = match r with Food -> closest_food | Wood -> closest_wood in
        Printf.printf "MOVING to resource 1\n";
        (move_action uid closest_r))
      else ()
    | None -> 
		  (* if there are resources, collect *)
		  if tile_has_resources v_tile (get_r_data ()) then
        (Printf.printf "COLLECTING action resource 2\n";
		    collect_action uid)
		  else if board_has_resource r_type then
      (* else, find closest resource tile and move there *)
		    ((* clearMove_action uid; *)
        let (closest_wood,closest_food) = get_closest_r v_pos in
        let closest_r =
          let wood_dist = (distance v_pos closest_wood)
          and food_dist = (distance v_pos closest_food) in
          if wood_dist < food_dist then closest_wood else closest_food in
          Printf.printf "MOVING to resource 2\n"; 
		    (move_action uid closest_r))
      else ()
     *)

(* Ansha functions *)
let getBarracks lst =
  let filter acc (bid,bt,h,t) =
    if bt = Barracks then (bid,bt,h,t)::acc else acc in
    List.fold_left filter [] lst
let getVillagers lst =
  let filter acc (uid,ut,h,p) =
    if ut = Villager then (uid,ut,h,p)::acc else acc in
    List.fold_left filter [] lst
let makeUnit ut lst = 
  let filter = fun (bid,bt,_,_) -> 
    match bt with 
      Barracks -> spawn_action bid ut
    | _ -> () in
  List.iter filter lst
let positionInTile pos (a,b) =
  let x,y = tile_of_pos pos in
    x=a && y=b
