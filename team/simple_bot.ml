open Team
open Definitions
open Constants
open Util
open Constants_helper
open Team_helper

let _ = Random.self_init ()

let get_random_index lst =
  Random.int (List.length lst)
    
let get_random_id lst = 
  let idify (id,_,_,_) = id in
  let ids = List.map idify lst in
  List.nth ids (get_random_index ids)
    
let count = ref 0 
(*let nextBuildingLoc = ref None*)

(*THIS IS THE ONLY METHOD YOU NEED TO COMPLETE FOR THE BOT*)
(*Make sure to use helper funcitons*)

(* high level strategy: attack all villagers *)
(* attack the enemy with priority as such: units, villagers, buildings. *)
(* as soon as enough resources are gathered, create as many pikemen *)
(* as possible to swarm the other team. make half the villages collect*)
(* wood, half collect food and if uneven number then check which resource*)
(* is more necessary and collect more of that. *)
(* This strategy does NOT: care about upgrades, other types of units,*)

(* initial collection as usual, then first barrack is created by the*)
(* villager closest to the home towncenter and it is created next to the*)
(* towncenter. then, spawn pikemn like crazy from this barrack forever,*)
(* but when enough resources for another barrack, send 1 pikeman with 1*)
(* villager to go create the next x barracks at the enemy towncenter!*)
(* kill all units that attack the villager. have accompanyming soldiers.*)
(* then spam spawn pikemen to kill all towncenter of enemy. keep 3 pikemen*)
(* at towncenter to defend the castle. at time 5, there must be 1 pikeman at *)
(* towncenter at all times, at time 10 there must be 2, at time 15 and beyond*)
(* there must be 3 at all times. *)
let bot c =
  (* initially, send villagers to collect wood and food *)
  (* make sure to get up to 5 villagers, then create a barrack*)
  
  (* send 2 villagers to get wood, 1 to get food: enough for 1 barrack, 1 pikeman *)
  (* when enough for barrack, send 1 villager that's currently getting wood *)
  (* to create the barrack. when done, spawn as many pikemen as possible. *)
  
  (* gather initial data from game *)
  let mine = ref None in
  let other = ref None in
  let r_lst = ref [] in
  let _ = (match (get_status GameStatus),(get_status ResourceStatus) with
      | GameData(t1,t2,t),ResourceData(rdl) ->
        if c=Red then (mine:=Some t1; other:=Some t2) 
      else (mine:=Some t2; other:=Some t1);
      r_lst := rdl
    | _,_ -> failwith "No data") in
    let (s1,udl1,bdl1,a1,fc1,wc1,u1) = 
    match !mine with
      | Some (s1,udl1,bdl1,a1,fc1,wc1,u1) -> (s1,udl1,bdl1,a1,fc1,wc1,u1) 
      | None -> failwith "No data" in
    let (s2,udl2,bdl2,a2,fc2,wc2,u2) = 
    match !other with
      | Some (s2,udl2,bdl2,a2,fc2,wc2,u2) -> (s2,udl2,bdl2,a2,fc2,wc2,u2)
      | None -> failwith "No data" in
  
  (* get IDs of the three initial villagers *)
  let uid_lst = List.fold_left (fun acc (id,_,_,_) -> id::acc) [] udl1 in
  let (tc_id,_,_,tc_tile) = List.find (fun (_,t,_,_) -> t=TownCenter) bdl1 in
  let (opp_tc_id,_,_,opp_tc_tile) =
    List.find (fun (_,t,_,_) -> t=TownCenter) bdl2 in
  
  let cannot_create_barrack () : bool =
    let (_,_,_,_,f_cnt,w_cnt,_) = get_t_data c in
    Printf.printf "CHECKING barrack resources 1\n";
    let (barr_f,barr_w) = cBARRACKS_COST in
    Printf.printf "CHECKING barrack resources 2\n";
    ((w_cnt <= barr_w) || (f_cnt <= barr_f)) in
  while cannot_create_barrack () do
	  Printf.printf "COLLECTING resources\n";
    (match uid_lst with
        | v1::v2::v3::t ->
          collect_resources v1 (Some Wood) c;
          collect_resources v2 (Some Wood) c;
          collect_resources v3 (Some Food) c;
(*          build_action v1;*)
          Printf.printf "DONE with loop\n"
        | _ -> ());
    Printf.printf "BOUT to leave the while\n";
  done;
  
  
(*  let cannot_create_barrack () : bool =          *)
(*    let (_,_,_,_,f_cnt,w_cnt,_) = get_t_data c in*)
(*    let (barr_f,barr_w) = cBARRACKS_COST in      *)
(*    ((w_cnt <= barr_w) || (f_cnt <= barr_f)) in  *)
(*  while cannot_create_barrack () do              *)
(*    (match uid_lst with                          *)
(*        | v1::v2::v3::t ->                       *)
(*          collect_resources v1 (Some Wood) c;    *)
(*          collect_resources v2 (Some Wood) c;    *)
(*          collect_resources v3 (Some Food) c     *)
(*        | _ -> ())                               *)
(*  done;                                          *)
  
  
  
  Printf.printf "READY to build barracks\n";
  
  let (vid,vp) = get_closest_villager opp_tc_tile c in
  let r_lst = get_r_data() in
  let (_,_,b_lst,_,_,_,_) = get_t_data c in
  let enemy_b_lst = ref [] in
  let _ = match c with
    | Red -> (let (_,_,lst,_,_,_,_) = get_t_data Blue in enemy_b_lst := lst)
    | Blue -> (let (_,_,lst,_,_,_,_) = get_t_data Red in enemy_b_lst := lst) in
  let total_b_lst = b_lst @ (!enemy_b_lst) in 
  let new_tile = get_new_barracks_location (tile_of_pos vp) total_b_lst r_lst in
  Printf.printf "Moving %d to (%d,%d)\n" vid (fst new_tile) (snd new_tile);
(*  let _ = send_action (QueueMove(vid, position_of_tile new_tile)) vid in*)
  move_action vid (position_of_tile new_tile);
  
  let get_curr_pos vid =
    let (_,u_lst,_,_,_,_,_) = get_t_data c in
    let (_,_,_,u_pos) = List.find (fun (id,_,_,_) -> id = vid) u_lst in
    u_pos in
  while not ((tile_of_pos (get_curr_pos vid)) = new_tile) do () done;
  
(*  let _ = send_action (QueueBuild(vid,Barracks)) vid in*)
  build_action vid;

  
  while true do ()

    
  done

let () = start_bot bot
