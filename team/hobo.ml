open Team
open Definitions
open Constants
open Util
open Constants_helper

let _ = Random.self_init ()

let get_random_index lst =
  Random.int (List.length lst)
    
let get_random_id lst = 
  let idify (id,_,_,_) = id in
  let ids = List.map idify lst in
  List.nth ids (get_random_index ids)
    
let count = ref 0 

(*THIS IS THE ONLY METHOD YOU NEED TO COMPLETE FOR THE BOT*)
(*Make sure to use helper funcitons*)

(* high level strategy: attack all villagers *)
(* attack the enemy with priority as such: units, villagers, buildings. *)
(* as soon as enough resources are gathered, create as many pikemen *)
(* as possible to swarm the other team. make half the villages collect*)
(* wood, half collect food and if uneven number then check which resource*)
(* is more necessary and collect more of that. *)
(* This strategy does NOT: care about upgrades, other types of units,*)

(* initial collection as usual, then first barrack is created by the*)
(* villager closest to the home towncenter and it is created next to the*)
(* towncenter. then, spawn pikemn like crazy from this barrack forever,*)
(* but when enough resources for another barrack, send 1 pikeman with 1*)
(* villager to go create the next x barracks at the enemy towncenter!*)
(* kill all units that attack the villager. have accompanyming soldiers.*)
(* then spam spawn pikemen to kill all towncenter of enemy. keep 3 pikemen*)
(* at towncenter to defend the castle. at time 5, there must be 1 pikeman at *)
(* towncenter at all times, at time 10 there must be 2, at time 15 and beyond*)
(* there must be 3 at all times. *)
let bot c =
  (* initially, send villagers to collect wood and food *)
  (* make sure to get up to 5 villagers, then create a barrack*)
  
  (* send 2 villagers to get wood, 1 to get food: enough for 1 barrack, 1 pikeman *)
  (* when enough for barrack, send 1 villager that's currently getting wood *)
  (* to create the barrack. when done, spawn as many pikemen as possible. *)
  
  
  while true do
    (* maintain balance of 5 total villagers at all times, 2 for wood 3 for food *)
    
    (* spawn pikemen and attack villagers of other team *)  
    (* send half to attack villagers half to attack units? *)
    
    
    
    
    
    
(*    let talk_action = Talk("Talk: " ^ (string_of_int !count)) in    *)
(*        let audio_action = Talk(string_of_int !count) in            *)
(*        count := (mod) (!count + 1) 40;                             *)
(*        let b = Random.int 100 in                                   *)
(*        let action = if b < 10 then audio_action else talk_action in*)
(*    let res = send_action action 0 in                               *)
(*    let _ = match res with                                          *)
(*      | Success -> print_endline ("Talk Success!")                  *)
(*      | Failed  -> print_endline ("Talk Failed") in                 *)
(*    Thread.delay 1.0                                                *)
  done

let () = start_bot bot
