@ECHO off
SET argC=0
FOR %%x in (%*) do (
	SET /A argC+=1
)
IF %argC% == 1 (
	ocamlc -o %1.exe -I +threads -I ../game -I ../shared unix.cma threads.cma str.cma ../shared/thread_pool.mli ../shared/thread_pool.ml ../shared/connection.mli ../shared/connection.ml ../shared/constants.ml ../team/constants_helper.ml ../shared/definitions.ml ../shared/util.ml team.ml team_helper.ml %1.ml
) ELSE (
	echo "usage: %0 <team-name>"
)

