open Constants
open Definitions

let attack_of (u: unit_type) : int option =
  match u with
    | Archer -> Some cARCHER_ATTACK
    | EliteArcher -> Some cELITE_ARCHER_ATTACK
    | Pikeman -> Some cPIKEMAN_ATTACK
    | ElitePikeman -> Some cELITE_PIKEMAN_ATTACK
    | Knight -> Some cKNIGHT_ATTACK
    | EliteKnight -> Some cELITE_KNIGHT_ATTACK
    | _ -> None

let range_of (u: unit_type) : float option =
  match u with
    | Archer -> Some cARCHER_RANGE
    | EliteArcher -> Some cELITE_ARCHER_RANGE
    | Pikeman -> Some cPIKEMAN_RANGE
    | ElitePikeman -> Some cELITE_PIKEMAN_RANGE
    | Knight -> Some cKNIGHT_RANGE
    | EliteKnight -> Some cELITE_KNIGHT_RANGE
    | _ -> None

let cooldown_of (u: unit_type) : float =
  match u with
    | Archer -> cARCHER_COOLDOWN
    | EliteArcher -> cELITE_ARCHER_COOLDOWN
    | Pikeman -> cPIKEMAN_COOLDOWN
    | ElitePikeman -> cELITE_PIKEMAN_COOLDOWN
    | Knight -> cKNIGHT_COOLDOWN
    | EliteKnight -> cELITE_KNIGHT_COOLDOWN
    | Villager -> cVILLAGER_COOLDOWN

let speed_of (u: unit_type) : float =
  match u with
    | Archer -> cARCHER_SPEED
    | EliteArcher -> cELITE_ARCHER_SPEED
    | Pikeman -> cPIKEMAN_SPEED
    | ElitePikeman -> cELITE_PIKEMAN_SPEED
    | Knight -> cKNIGHT_SPEED
    | EliteKnight -> cELITE_KNIGHT_SPEED
    | Villager -> cVILLAGER_SPEED

let health_of (u: unit_type) : int =
  match u with
    | Archer -> cARCHER_HEALTH
    | EliteArcher -> cELITE_ARCHER_HEALTH
    | Pikeman -> cPIKEMAN_HEALTH
    | ElitePikeman -> cELITE_PIKEMAN_HEALTH
    | Knight -> cKNIGHT_HEALTH
    | EliteKnight -> cELITE_KNIGHT_HEALTH
    | Villager -> cVILLAGER_HEALTH

let spawncost_of (u: unit_type) : (int * int) =
  match u with
    | Archer | EliteArcher -> cSPAWN_ARCHER_COST
    | Pikeman | ElitePikeman -> cSPAWN_PIKEMAN_COST
    | Knight | EliteKnight -> cSPAWN_KNIGHT_COST
    | Villager -> cSPAWN_VILLAGER_COST

let upgradecost_of (u: unit_type) : (int * int) option =
  match u with
    | Archer -> Some cUPGRADE_ARCHER_COST
    | Pikeman -> Some cUPGRADE_PIKEMAN_COST
    | Knight -> Some cUPGRADE_KNIGHT_COST
    | _ -> None
