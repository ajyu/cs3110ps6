open Team
open Definitions
open Constants
open Util
open Constants_helper
open Team_helper

let _ = Random.self_init ()

let get_random_index lst =
  Random.int (List.length lst)
    
let get_random_id lst = 
  let idify (id,_,_,_) = id in
  let ids = List.map idify lst in
  List.nth ids (get_random_index ids)
    
let count = ref 0 
let nextBuildingLoc = ref None

let bot c =
  let mine = ref None in
  let other = ref None in
  let r_lst = ref [] in
  let _ = (match (get_status GameStatus),(get_status ResourceStatus) with
	  | GameData(t1,t2,t),ResourceData(rdl) ->
	    if c=Red then (mine:=Some t1; other:=Some t2) 
      else (mine:=Some t2; other:=Some t1);
      r_lst := rdl
    | _,_ -> failwith "No data") in
	let (s1,udl1,bdl1,a1,fc1,wc1,u1) = 
    match !mine with
      | Some (s1,udl1,bdl1,a1,fc1,wc1,u1) -> (s1,udl1,bdl1,a1,fc1,wc1,u1) 
      | None -> failwith "No data" in
	let (s2,udl2,bdl2,a2,fc2,wc2,u2) = 
    match !other with
      | Some (s2,udl2,bdl2,a2,fc2,wc2,u2) -> (s2,udl2,bdl2,a2,fc2,wc2,u2)
      | None -> failwith "No data" in
  
  (* get IDs of the three initial villagers *)
  let uid_lst = List.fold_left (fun acc (id,_,_,_) -> id::acc) [] udl1 in
  let (tc_id,_,_,tc_tile) = List.find (fun (_,t,_,_) -> t=TownCenter) bdl1 in
  let (opp_tc_id,_,_,opp_tc_tile) =
    List.find (fun (_,t,_,_) -> t=TownCenter) bdl2 in
  
  (* send to closest wood and foods respectively in a while loop until enough for barrack *)
  let cannot_create_barrack () : bool =
    let (_,_,_,_,f_cnt,w_cnt,_) = get_t_data c in
    let (barr_f,barr_w) = cBARRACKS_COST in
    ((w_cnt <= barr_w) || (f_cnt <= barr_f)) in
  spawn_action tc_id Villager;
  spawn_action tc_id Villager;
  spawn_action tc_id Villager;
  spawn_action tc_id Villager;
  spawn_action tc_id Villager;
  while cannot_create_barrack () do
    (match uid_lst with
	    | v1::v2::v3::t ->
	      collect_resources v1 (Some Wood) c;
	      collect_resources v2 (Some Wood) c;
	      collect_resources v3 (Some Food) c
	    | _ -> ())
  done;

	let (_,udl1,_,_,_,_,_) = get_t_data c in
  let uid_lst = List.fold_left (fun acc (id,_,_,_) -> id::acc) [] udl1 in
  let collectors = match uid_lst with h::t -> t | _ -> [] in
  let (v_id,_,_,v_pos) = List.hd udl1 in
  let total_b_data = bdl1 @ bdl2 in
  (* move villager to correct location for building *)
  let target_tile =
    get_new_barracks_location (tile_of_pos v_pos) total_b_data !r_lst in

	move_action v_id (position_of_tile target_tile);
  let is_not_at_target () : bool =
    let (_,udl1,_,_,_,_,_) = get_t_data c in
	  let (_,_,_,v_pos) = List.find (fun (id,_,_,_) -> id=v_id) udl1 in
    not ((tile_of_pos v_pos) = target_tile) in

  build_action v_id;

  
  while true do
    match (get_status GameStatus),(get_status ResourceStatus) with
      GameData(t1,t2,t),ResourceData(rdl) ->
        (* Upgrades *)
        let _ = send_action (Upgrade(AgeUpgrade)) 0 in
        let _ = send_action (Upgrade(UnitUpgrade(Pikeman))) 0 in
        let _ = send_action (Upgrade(UnitUpgrade(Knight))) 0 in
        let mine,other = if c=Red then t1,t2 else t2,t1 in
        let (s1,udl1,bdl1,a1,fc1,wc1,u1) = mine in
        let (s2,udl2,bdl2,a2,fc2,wc2,u2) = other in
        let villagers = getVillagers udl1 in
        let villagerCollect () =
          let mkCollect (uid,_,_,_) = collect_resources uid (None) c in
          List.iter mkCollect villagers in 

        (* 1. Unit spawning *)
        let _ = makeUnit EliteKnight bdl1 in
        let _ = makeUnit Pikeman bdl1 in

        (* 2. Barrack building:
         *    a. if enough resources for Barracks and a Villager exists at next
         *    building location, then tell Villager to build
         *    b. else if enough resources for Barracks, send a Villager to next
         *    building location
         *    c. else Villagers collect
         *)
        let _ = 
          let _ = villagerCollect () in
          let _ = if List.length bdl1 <=2 then
          let f,w = cBARRACKS_COST in
          let newBarracksLoc = match !nextBuildingLoc with
              None -> 
                let vid,vp = get_closest_villager opp_tc_tile c in
                let newloc = get_new_barracks_location (tile_of_pos vp) (bdl1@bdl2) rdl in
                nextBuildingLoc := Some(newloc) ;
                newloc
            | Some(tile) -> tile in
          let rec villagerAtBuildingLocation = function
              [] -> (false,(0,Villager,0,(0.,0.)))
            | h::t -> 
              let (uid,ut,_,p) = h in
                if positionInTile p newBarracksLoc then (true,h)
                else villagerAtBuildingLocation t in
          let (heExists,(thisIsHim,_,_,_)) = villagerAtBuildingLocation villagers in
          let _ = if heExists then
            match send_action (QueueBuild(thisIsHim,Barracks)) thisIsHim with
                    Success -> nextBuildingLoc := None
                  | _ -> () in
          let vid = fst (get_closest_villager newBarracksLoc c) in
            move_action vid (position_of_tile newBarracksLoc) in

            if List.length villagers <= 15 then
            spawn_action tc_id Villager in

        (* 3. Pikeman attacking *)
        let _ =
          let mkAttack (uid,ut,_,p) =
            clearMove_action uid;
            move_action uid (position_of_tile opp_tc_tile) ;
            attack_action uid (Building(opp_tc_id)) in
          let nonV,_ = List.partition (fun (_,ut,_,_) -> not (ut=Villager)) udl1 in
          List.iter mkAttack nonV in
          ()
    | _ -> ()

  done

let () = start_bot bot
