open Team
open Definitions
open Constants
open Util

let _ = Random.self_init ()

let get_random_index lst =
  Random.int (List.length lst)
    
let get_random_id lst = 
  let idify (id,_,_,_) = id in
  let ids = List.map idify lst in
  List.nth ids (get_random_index ids)
    
let count = ref 0 

let getBarracks lst =
  let filter acc (bid,bt,h,t) =
    if bt = Barracks then (bid,bt,h,t)::acc else acc in
    List.fold_left filter [] lst
let getVillagers lst =
  let filter acc (uid,ut,h,p) =
    if ut = Villager then (uid,ut,h,p)::acc else acc in
    List.fold_left filter [] lst
let makePikeman lst = 
  let filter = fun (bid,bt,_,_) -> 
    match bt with 
      Barracks -> spawn_action bid Pikeman
    | _ -> () in
  List.iter filter lst
let positionInTile pos (a,b) =
  let x,y = tile_of_pos pos in
    x=a && y=b
let nextBuildingLoc = ref None



(*THIS IS THE ONLY METHOD YOU NEED TO COMPLETE FOR THE BOT*)
(*Make sure to use helper funcitons*)

(* high level strategy: attack all villagers *)
(* attack the enemy with priority as such: units, villagers, buildings. *)
(* as soon as enough resources are gathered, create as many pikemen *)
(* as possible to swarm the other team. make half the villages collect*)
(* wood, half collect food and if uneven number then check which resource*)
(* is more necessary and collect more of that. *)
(* This strategy does NOT: care about upgrades, other types of units,*)

(* initial collection as usual, then first barrack is created by the*)
(* villager closest to the home towncenter and it is created next to the*)
(* towncenter. then, spawn pikemn like crazy from this barrack forever,*)
(* but when enough resources for another barrack, send 1 pikeman with 1*)
(* villager to go create the next x barracks at the enemy towncenter!*)
(* kill all units that attack the villager. have accompanyming soldiers.*)
(* then spam spawn pikemen to kill all towncenter of enemy. keep 3 pikemen*)
(* at towncenter to defend the castle. at time 5, there must be 1 pikeman at *)
(* towncenter at all times, at time 10 there must be 2, at time 15 and beyond*)
(* there must be 3 at all times. *)
let bot c =
  (* initially, send villagers to collect wood and food *)
  (* make sure to get up to 5 villagers, then create a barrack*)
  
  (* send 2 villagers to get wood, 1 to get food: enough for 1 barrack, 1 pikeman *)
  (* when enough for barrack, send 1 villager that's currently getting wood *)
  (* to create the barrack. when done, spawn as many pikemen as possible. *)
  while true do
    (* 1. Spawn as many pikemen as possible from your barracks
     * 2. more villagers for food than wood, exact number and ratio to
     * be determined
     * 3. Make barracks one at a time around the enemy towncenter
     *    a) have pikemen accompany villagers
     * 4. Keep pikemen at home towncenter to attack oncoming units
     *
     * Pikemen attack enemey towncenter
     * When contronted by opponents, attack back
     *
     * Functions required:
     * getClosestVillager
     * get_new_barracks_location tile buildinglist
     *)
    match (get_status GameStatus),(get_status ResourceStatus) with
      GameData(t1,t2,t),ResourceData(rdl) ->
        let mine,other = if c=Red then t1,t2 else t2,t1 in
        let (s1,udl1,bdl1,a1,fc1,wc1,u1) = mine in
        let (s2,udl2,bdl2,a2,fc2,wc2,u2) = other in
        let villagers = getVillagers udl1 in
        let villagerCollect () =
          let mapToResource (uid,ut,h,p) = 
            let w,f = get_closest_r p in
            let wd,fd = distance w p , distance f p in
            let w,f = tile_of_pos w , tile_of_pos f in
            let rt,rd = if wd < fd then Wood,fd-wd else Food,wd-fd in
              (uid, w, f, rd, rt) in
          let distList = List.map mapToResource villagers in
          let count (wc,fc) (uid,_,_,_,rt) = match rt with
            Food -> (wc,fc+1)
          | Wood -> (wc+1,fc) in
          let wooders,fooders = List.fold_left count (0,0) closestType in
          let distList = if fooders / (fooders+wooders) < 2/3 then
            let whichVillagerToChange (uid2,rd2) (uid,w,f,rd,rt) = 
              if rd < rd2 then (uid,rd) else (uid2,rd2) in
            let uidToChange,_ = List.fold_left whichVillagerToChange (0,max_int) in
            let minChange acc (uid,w,f,rd,rt) = if uid = uidToChange then
             (uid,w,f,rd,Food)::acc else acc in
              List.fold_left minToChange [] distList in
          let mkCollect (uid,w,f,rd,rt) =
            collect_action uid in
            List.iter mkCollect distList in




        (* 1. Pikeman spawning *)
        let _ = makePikeman bdl1 in

        (* 2. Barrack building:
         *    a. if enough resources for Barracks and a Villager exists at next
         *    building location, then tell Villager to build
         *    b. else if enough resources for Barracks, send a Villager to next
         *    building location
         *    c. else Villagers collect
         *)
        let _ = 
          let _ = villagerCollect () in
          let f,w = cBARRACKS_COST in
          let newBarracksLoc = match !nextBuildingLoc with
              None -> nextBuildingLoc :=
                get_new_barracks_location (tile_of_pos vp) (bdl1@bdl2) rdl ;
                !nextBuildingLoc
            | Some(tile) -> tile in
          let rec villagerAtBuildingLocation = function
              [] -> (false,(0,Villager,0,(0.,0.)))
            | h::t -> 
              let (uid,ut,_,p) = h in
                if positionInTile p newBarracksLoc then (true,h)
                else villagerAtBuidingLocation t in
          let (heExists,(thisIsHim,_,_,_)) = villagerAtBuildingLocation villagers in

            if fc1-f>=0 && wc1-w>=0 && heExists then
              build_action thisIsHim
            else if fc1-f>=0 && wc1-w>=0 then
              move_action (get_closest_villager newBarracksLoc c) (position_of_tile newBarracksLoc)
            else if List.length villagers >= 6 then
              nextBuildingLoc := None
            else nextBuildinLoc := None ; spawn_action tc_id Villager in

        (* 3. Pikeman attacking *)
        let _ =
          let mkAttack (uid,ut,_,p) =
            if ut=Villager then () else
            clear_action uid ; 
            move_action uid (position_of_tile opp_tc_tile) ;
            attack_action uid opp_tc_id in
            List.iter mkAttack uid in
          ()
    | _ -> ()


          

    
    
    
    
    
    
  done

let () = start_bot bot
