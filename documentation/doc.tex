\documentclass{article}

\usepackage[cm]{fullpage}
\usepackage{parskip}

\author{Ansha Yu (ay226) \and Horace Chan (hhc39)}
\title{CS3110 PS6: Age of Upson: Game}

\begin{document}
\maketitle

\section{Summary}
This document is an overview of our Age of Upson game implementation decisions. The main parts of our game reside in \verb!game.ml! and \verb!state.ml!. \verb!Game.ml! contains the majority of the appropriate checks that are required before a command is sent to change the state. \verb!State.ml! performs that functions necessary to change the state, and it also contains additional checks appropriate for the game. The biggest challenges was mainly in testing the bot. Although we did incremental testing as we as we wrote our functions, the majority of our issues came in resolving issues that we didn't catch until we saw things happen on the GUI.

\section{Specification}
\subsection{state.mli}
\begin{verbatim}
open Definitions
(* Gets the relevant information required *)
(* These functions are used to get information about the state
 * from the game and perform appropriate checks before changing state *)
val get_gctown_center : color -> building_data
val getUnitId : unit_gcid -> unit_info option
val getTeamUnits : color -> unit_gcdata list
val getBuildingId : building_gcid -> building_data option
val getBuildingTile : tile -> building_gcdata option
val getTeamBuildings : color -> building_gcdata list
val getResourceTile : tile -> resource_gcdata option
val inRange : unit_gcid -> attackable_object -> bool

(* Helper functions *)
(* Modifies the resources, score, or both of a team, updating the gui *)
val addResources : state -> color -> food_count -> wood_count -> unit
val addResourcesNoScore : state -> color -> food_count -> wood_count -> unit
val addScore : state -> color -> int -> unit
(* helper function to check if this tile is blocked by something *)
(* returns true if the tile is clear, false if there is a resource *)
(* or building blocking it *)
val check_tile : tile -> bool

(* Initiates resources to start values *)
val initResources : unit -> unit
(* Initiates all states to primitive values *)
val initState : unit -> state

(* Initiates towncenter and villagers *)
val initObjects : unit -> unit

(* The following sets of functions adds the respective actions to the queue *)
val addBuild : unit_gcid -> unit
(* Attacks are added without regard to range *)
val addAttack : unit_gcid -> attackable_object -> unit
(* Adds intermediate moves to queue *)
val addMove : unit_gcid -> vector -> unit
val clearAttack : unit_gcid -> unit
val clearMove : unit_gcid -> unit
(* The following happen IMMEDIATELY and is NOT part of increment_time*)
(* Assumes that spawns have already been checked for appropriate building
 * type in game *)
val addSpawn : state -> building_gcid -> unit_type -> unit
(* To process a collect:
 * 1. Make sure the the unit isn't under a cooldown time!
 * 2. Decrement the amount of resources on that tile
 * 3. Increment the resources the team has
 * 4. Process the new cooldown time for the unit
 *)
val addCollect : state -> unit_gcid -> unit


(* Performs the appropriate upgrades *)
(* Upgrades need to be checked in game before these functions are called *)
val upgradeAge : state -> color -> unit
val upgradeUnit : state -> color -> unit_gctype -> unit

(* Handling attacks:
 * 1. Go down queue until empty, only carrying out attacks 
 *    a) within range
 *    b) without cooldown time
 * 2. For legal attacks, decrement opponent health
 * 3. Increase team score
 * 4. Add cooldown time
 * 5. Empty queue
 *)
 val handleAttacks : state -> curr_time -> unit
 (* Handles dead units, buildings, and resources *)
 (* When a building cannot be completed due to the removal of a villager, it is
 * 1. Removed from in_progress 
 * 2. Removed from buildings
 * 3. Removed from building_tiles*)
 val handleDead : state -> curr_time -> unit
 val handleCompleteBuild -> curr_time -> unit

 (* When a building is started, it is
 * 0a. Checked for adequate resources
 * 0b. Villager checked for cooldown time
 * 0c. Checked for building location again
 * 1. Removed from builds
 * 2. Added to in_progress
 * 3. Added to buildings (given a building_id & building_data)
 * 4. Building_tiles is updated with the connection from tile to building_id
 *)
 val handleAddBuild : state -> curr_time -> unit
 (* Check that if villager && villager is building, then it shouldn't move*)
 val handleMove : curr_time -> unit

(* Spawned has to be reset to (false,false) *)
val increment_gctime : state -> unit
\end{verbatim}


\section{Design and Implementation}
\subsection{Modules}
The \verb!Game! module includes the majority of the checks required before the game state is changed. These checks include but are not limited by the following. All checks not handled by \verb!Game! are handled by \verb!State!. Some checks and actions are handled in \verb!State! (a big example of this is initResources and all its helper functions). This decision was made in order to primarily simplify our code, because some of these functions need to access the state hashtables very often. 

\begin{enumerate}
  \item Collects are only executed by Villagers 
  \item Attacks are only carried out by non-Villagers are only if a unit is in range
  \item Builds only occur if there are no other resources or buildings in progress or built in the tiles that the building will occupy.
  \item Spawns only occur when a spawn is of the same age class as the team.
  \item Upgrades only occur when the resource count is sufficient. Age upgrades only occur when the team is in \verb!DarkAge!.
\end{enumerate}

\subsection{Architecture}
The game module depends on the correct implementation of the state module. Ideally, all checks happen in \verb!Game!, and \verb!State! solely make changes to the state. However, this is no entirely possible because certain checks are more appropriate with the level of access on has in state. For example, when queuing a build, resource checking would be ideal, but because during a time step, resources can be subtracted due to spawns and upgrades, the resource checking must happen in \verb!State! when the building queue is actually handled.

The type we chose for our game is a \verb!(state ref * Mutex.t!. In this manner, one copy of the game is passed to the server, and the Mutex is used to control access to the state . A state is in fact a game\_state ref, which mutably holds a state. Even though many aspects of the state is stored in mutable data structures such as hashtables and queues, the state contains crucial information about score, health, time, and upgrades.

In state, we also keep a ref for the towncenter of each team so that it is easy to check for its existence when checking for game over conditions. We also keep a ref for whether a team has already spawned a unit during a timestep. This ref is reset at the end of each timestep. 

\subsection{Code Design}
 We make extensive use of OCaml's \verb!Hashtbl! in order to keep track of our units, buildings, and resources. The following is a list of the different hashtables used to track state in the game. Only one of each exist per state.

 \begin{enumerate}
\item units : (unit\_gcid,(unit\_data*moves*attacks*color*timer)) Hashtbl.t
\item resources : (tile,resource\_gcdata) Hashtbl.t 
\item buildings : (building\_gcid,building\_data*color) Hashtbl.t 
\item building\_gctiles : (tile,building\_id) Hashtbl.t
\item in\_gcprogress: (unit\_id,building\_data) Hashtbl.t
  \end{enumerate}

  Queues are also used extensively to keep track of attacks, moves, and builds. With the exception of moves, all Queues should be empty by the end of the timestep, because invalid moves are discarded. There is one attack and move queue for each unit. The attack queue for Villagers should be theoretically empty at the each timestep. 
  
\begin{enumerate}
\item builds : unit\_id Queue.t 
\item attacks : attackable\_object Queue.t
\item moves : position Queue.t
\end{enumerate}

\subsection{Programming}
Horace mainly coded \verb!Game!, and Ansha mainly coded \verb!State!, except for the \verb!initState! and \verb!initObjects! functions which Horce completed in \verb!State!. We first completed \verb!state.mli!, which helped us think about the functions and structures required in \verb!State!. We used a Google Docs spreadsheet in order to alert each other on changes and tasks that we wanted each other to complete. We used BitBucket for our Git host.

\section{Testing}
Our plan for testing is to do unit tests on all of the \verb!action!s that are defined in \verb!definitions.ml!. We assigned half of the list to each partner, with the agreement that we will also do overlapping tests in order to cover cases where actions overlap.

\section{Known Problems}

\section{Comments}
We each spent around 40 hours on this assignment. The coding was evenly divided between Horace and Ansha, each of them writing one module. Horace did a greater share of the debugging, and Ansha completed the design document. The advice given to us before the start of this assignment was appropriate. The hard part about this assignment is the debugging, because one cannot be sure whether an error is due to an inconsistency with the GUI or due to incorrect game logic. We like that this assignment was open ended and allowed us to put in more thought with the module design process. The necessary functions in State were not given to us, and we needed to think about a logical structure for the modules used in order to encapsulate our data. We disliked that the GUI/game logic error divide was not clear. If we could change something about this assignment, then we would give ourselves more time to finish.

\end{document}
